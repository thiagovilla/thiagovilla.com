import React from "react";
import { graphql } from "gatsby";
import MainLayout from "../layout/MainLayout";

const DropboxTemplate = ({ data }) => {
  return (
    <MainLayout>
      <div
        dangerouslySetInnerHTML={{
          __html: data.dropboxNode.localFile.childMarkdownRemark.html,
        }}
      />
    </MainLayout>
  );
};

export const Head = ({ data }) => (
  <title>
    {data.dropboxNode.localFile.childMarkdownRemark.frontmatter.title}
  </title>
);

export const query = graphql`
  query ($dbxPath: String!) {
    dropboxNode(dbxPath: { eq: $dbxPath }) {
      localFile {
        childMarkdownRemark {
          html
          frontmatter {
            title
          }
        }
      }
    }
  }
`;

export default DropboxTemplate;
