import React from "react";
import { graphql } from "gatsby";
import BaseLayout from "../layout/BaseLayout";

const PageTemplate = ({ data }) => {
  const { datoCmsPage } = data;

  return (
    <BaseLayout>
      <div dangerouslySetInnerHTML={{ __html: datoCmsPage.html }} />
    </BaseLayout>
  );
};

export const Head = ({ data }) => <title>{data.datoCmsPage.title}</title>;

export const query = graphql`
  query ($slug: String!) {
    datoCmsPage(slug: { eq: $slug }) {
      title
      html
    }
  }
`;

export default PageTemplate;
