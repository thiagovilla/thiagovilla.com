# Content

Use the filesystem for content. For now.

TO DO: remove when all external source plugins are restored.

# Source Plugins

This website uses the following external source plugins:

- [DatoCMS](#datocms)

## DatoCMS

DatoCMS is a cloud Content Management System.

**Content**: `Page` generic model (home page, about me, etc.).

**Docs Link:** https://www.gatsbyjs.com/plugins/gatsby-source-datocms/

**File/Folder Structure:** This is a cloud CMS plugin and therefore there is no file/folder structure.

**Prod/Draft Config:** Set `previewMode: true` in `gatsby-config.js` to render draft content.

**Auth:** Set `apiToken` to an API token from the Dato configuration panel.

**Page Creation:** Use collection routes with the `Page` model `slug` field.

The `/` slug ony renders to index _if_ there is no `index.js` file.

## Google Docs

**Content:** This plugin will be used to store blog posts (`/blog`).

**Docs Link:** https://www.gatsbyjs.com/plugins/gatsby-source-google-docs

**File/Folder Structure:** `published` and `draft` folders with `skip: true` to skip in slug.

**Prod/Draft Config:** Set `folder` to the `published` folder ID in production or to the **root folder** (not `draft`) otherwise.

**Auth:** Follow the plugin documentation for access token generation instructions.

**Page Creation:** Set `createPages: true` in `gatsby-config.js`.

## Dropbox

if no folder then Click Generate Acces Token"

**Content:** Docs folder (`/docs`)

**Docs Link:** https://www.gatsbyjs.com/plugins/gatsby-source-dropbox/

**File/Folder Structure:** Just put files and folders in the app root folder.

**Prod/Draft Config:** Exclude `.dft.md` from the `extensions` config in production.

**Auth:** Access token must be generated at runtime.

**Page Creation:** Via Gatsby Node API because `{DropboxNode.dbxPath}.js` includes the file extension.

# Transformer Plugins

**gatsby-transformer-remark:** parses markdown into HTML. Required by: Google Docs. Version 6 requires Gatsby 5. Use version 5 with Gatsby 4.
