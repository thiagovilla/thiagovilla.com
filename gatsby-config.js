/**
 * @type {import('gatsby').GatsbyConfig}
 */
require("dotenv").config();

if (!process.env.DROPBOX_ACCESS_TOKEN) {
  throw new Error("Undefined or empty DROPBOX_ACCESS_TOKEN");
}

const DRAFT_ENV = process.env.DRAFT_ENV === "true";
module.exports = {
  siteMetadata: {
    title: `Thiago Villa`,
    siteUrl: `https://thiagovilla.com`,
  },
  plugins: [
    "gatsby-plugin-image",
    {
      resolve: "gatsby-source-filesystem",
      options: {
        name: "images",
        path: "./src/images/",
      },
      __key: "images",
    },
    {
      resolve: "gatsby-source-filesystem",
      options: {
        name: "pages",
        path: "./src/pages/",
      },
      __key: "pages",
    },
    {
      resolve: "gatsby-source-datocms",
      options: {
        apiToken: process.env.DATOCMS_API_TOKEN,
        previewMode: DRAFT_ENV,
      },
    },
    {
      resolve: "gatsby-source-google-docs",
      options: {
        folder: process.env.GOOGLE_DOCS_FOLDER_ID,
        createPages: true,
      },
    },
    "gatsby-transformer-remark",
    {
      resolve: "gatsby-source-dropbox",
      options: {
        accessToken: process.env.DROPBOX_ACCESS_TOKEN,
        extensions: DRAFT_ENV ? [".dft.md", ".md"] : [".md"],
        recursive: true,
      },
    },
  ],
};
