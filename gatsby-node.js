const path = require("path");

exports.createPages = async ({ graphql, actions }) => {
  const { createPage } = actions;
  const result = await graphql(`
    {
      allDropboxNode {
        edges {
          node {
            dbxPath
          }
        }
      }
    }
  `);

  result.data?.allDropboxNode.edges.forEach(({ node: { dbxPath } }) => {
    createPage({
      path: path.dirname(dbxPath) + "/" + path.basename(dbxPath, ".md"),
      component: path.resolve(`./src/templates/dropbox.js`),
      context: { dbxPath },
    });
  });
};
